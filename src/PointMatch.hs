{-# LANGUAGE OverloadedStrings #-}
module PointMatch where

import CodeWorld
import Data.Text (pack)

-- | Main tile of the game.
data Tile = 
  Key TileColor 
  | Tile (Maybe TileColor)
  deriving (Eq)

data TileForm = S | VL | GL | UC | LC | RC | BC | UL | UR | BL | BR | ULR | BLR | LBU | RBU | X
  deriving (Eq)

-- | Color choice of the tile.
data TileColor
  = ColorRed | ColorGreen | ColorBlue
  | ColorPink | ColorPurple | ColorYellow | ColorCyan | ColorOrange
  | ColorWhite | ColorGray | ColorBlack | ColorBrown
  | ColorDarkRed | ColorDarkGreen
  | ColorLightBlue | ColorLightYellow
  deriving (Eq, Show)

-- | Get CodeWorld.Color from TileColor.
tileColor :: TileColor -> CodeWorld.Color
tileColor dc =
  case dc of
    ColorRed         -> CodeWorld.red
    ColorBlue        -> CodeWorld.blue
    ColorGreen       -> CodeWorld.green
    ColorPurple      -> CodeWorld.purple
    ColorYellow      -> CodeWorld.yellow
    ColorPink        -> CodeWorld.pink
    ColorBrown       -> CodeWorld.brown
    ColorOrange      -> CodeWorld.orange
    ColorCyan        -> CodeWorld.RGB 0.0 1.0 1.0
    ColorWhite       -> CodeWorld.white
    ColorGray        -> CodeWorld.gray
    ColorBlack       -> CodeWorld.black
    ColorDarkRed     -> CodeWorld.dark CodeWorld.red
    ColorDarkGreen   -> CodeWorld.dark CodeWorld.green
    ColorLightBlue   -> CodeWorld.light CodeWorld.blue
    ColorLightYellow -> CodeWorld.light CodeWorld.yellow

tileForm :: TileForm -> Picture
tileForm tf =
  case tf of
    S   -> solidRectangle 0.5 0.5
    VL  -> solidRectangle 0.5 1
    GL  -> solidRectangle 1 0.5
    UC  -> translated 0 (-0.125) (solidRectangle 0.5 0.75)
    LC  -> translated (-0.125) 0 (solidRectangle 0.75 0.5)
    RC  -> translated 0.125 0 (solidRectangle 0.75 0.5)
    BC  -> translated 0 0.125 (solidRectangle 0.5 0.75)
    UL  -> tileForm UC <> tileForm LC
    UR  -> tileForm UC <> tileForm RC
    BL  -> tileForm BC <> tileForm LC
    BR  -> tileForm BC <> tileForm RC
    ULR -> tileForm UC <> tileForm GL
    BLR -> tileForm BC <> tileForm GL
    LBU -> tileForm VL <> tileForm LC
    RBU -> tileForm VL <> tileForm RC
    X   -> tileForm VL <> tileForm GL

-- | State of the game.
data State = State [Board] Tile Bool Bool Int

-- | Board of the game.
type Board = [[Tile]]

-- | Draw one board tile at given coordinates.
drawTileAt :: Int -> Int -> Tile -> Board -> Picture
drawTileAt j i tile board = translated x y (rectangle 1 1 <> cellPicture)
  where
    x = fromIntegral i
    y = - fromIntegral j
    cellPicture = case tile of
      Key c         -> colored (tileColor c) (solidCircle 0.25)
      Tile Nothing  -> blank
      Tile (Just c) -> colored (tileColor c) (tileForm $ getFormForTileAt (i, j) c board)

getFormForTileAt :: (Int, Int) -> TileColor -> Board -> TileForm
getFormForTileAt (x, y) color board = form
  where
    upper = isTileColoredWith (getTileAt (x, y+1) board) color
    left  = isTileColoredWith (getTileAt (x-1, y) board) color
    right = isTileColoredWith (getTileAt (x+1, y) board) color
    lower = isTileColoredWith (getTileAt (x, y-1) board) color
    form  = case (upper, left, right, lower) of
      (True, False, False, False) -> UC
      (False, True, False, False) -> LC
      (False, False, True, False) -> RC
      (False, False, False, True) -> BC
      (True, False, False, True)  -> VL
      (False, True, True, False)  -> GL
      (True, True, False, False)  -> UL
      (True, False, True, False)  -> UR
      (False, True, False, True)  -> BL
      (False, False, True, True)  -> BR
      (True, True, True, False)   -> ULR
      (False, True, True, True)   -> BLR
      (True, True, False, True)   -> LBU
      (True, False, True, True)   -> RBU
      (True, True, True, True)    -> X
      _ -> S

isTileColoredWith :: Tile -> TileColor -> Bool
isTileColoredWith tile color = case tile of
  Tile (Just color')
    | color' == color -> True
    | otherwise       -> False
  Key color'
    | color' == color -> True
    | otherwise       -> False
  _                   -> False

-- | Draw board with a given state.
drawBoard :: State -> Picture
drawBoard (State [] _ _ _ _) = (lettering "Game over!")
drawBoard (State (board:_) _ finished _ score)
  | finished  = translated 0 5 (lettering "You won!") <> translated 0 4 (lettering $ pack ("Your score: " ++ show score)) <> picBoard
  | otherwise = picBoard
  where
    drawTileAt' j i tile = drawTileAt j i tile board
    rowPics row y = zipWith (drawTileAt' y) [0..] row
    drawBoardRow row y = foldl (<>) blank (rowPics row y)
    picsRow = zipWith (drawBoardRow) board [0..]
    picBoard = foldl (<>) blank picsRow

-- | Put one tile at given coordinates.
putTileAt :: (Int, Int) -> State -> State
putTileAt (x,y) s@(State (board:bs) currentTile finished allowDragging score)
  | finished = State bs (Tile Nothing) False False 0
  | isPointOutOfBoard (x, y) board = s
  | otherwise = State (updatedBoard:bs) nextTile nextFinished allowDragging newScore
  where
    (highSplitY, lowSplitY)   = splitAt y board
    selectedRow : _           = lowSplitY
    (leftSplitX, rightSplitX) = splitAt x selectedRow
    selectedTile : _          = rightSplitX
    
    updatedTile = case selectedTile of
      Key _         -> selectedTile
      Tile Nothing  -> case allowDragging of
        True -> currentTile
        False -> Tile Nothing
      Tile (Just c) -> case allowDragging of
        True -> currentTile
        False -> Tile (Just c)

    updatedRow = leftSplitX ++ [updatedTile] ++ drop 1 rightSplitX
    updatedBoard = highSplitY ++ [updatedRow] ++ drop 1 lowSplitY
    
    nextTile = case selectedTile of
      Key t -> Tile (Just t)
      _     -> currentTile

    newScore = case selectedTile of
      Tile _ -> score + 1
      _      -> score

    nextFinished = isGameFinished updatedBoard
      
-- | Check if the game is finished (all keys are matched).
isGameFinished :: Board -> Bool
isGameFinished board = and [isPointMatched' key | key <- getKeys board]
  where
    enumerate = zip [0..]
    getKeys board = do
      [((x, y), color)
        | (y, row)       <- enumerate board
        , (x, Key color) <- enumerate row]     
        
    isPointMatched' (p, color) = isPointMatched color board [p] p

-- | Check if the given key is matched.
isPointMatched :: TileColor -> Board -> [(Int, Int)] -> (Int, Int) -> Bool
isPointMatched c board visited (x, y)
  | isNotVisited (x, y) && isPointMatchKey (x, y) board c = True
  | otherwise = or [isPointMatched' neighbour
                    | neighbour <- neighbours
                    , isAvailable neighbour]
  where 
    neighbours = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
    
    isMatchKeyColor p = isPointMatchKey p board c
    isMatchTileColor p = isPointMatchTile p board (Tile (Just c))
    
    isOnBoard p    = not (isPointOutOfBoard p board)
    isNotVisited p = not (p `elem` visited)
    isMatchColor p = isMatchKeyColor p || isMatchTileColor p
    isAvailable p  = isOnBoard p && isNotVisited p && isMatchColor p
    
    isPointMatched' (x', y') = isPointMatched c board ((x, y) : visited) (x', y')

-- | Check if the given point is out of the game board.
isPointOutOfBoard :: (Int, Int) -> Board -> Bool
isPointOutOfBoard (x, y) board = 
  x >= length row || y >= length board || x < 0 || y < 0
  where
    row : _ = board
    
-- | Check if the given point is the key with the given color.
isPointMatchKey :: (Int, Int) -> Board -> TileColor -> Bool
isPointMatchKey p board c = isPointMatchTile p board (Key c)
    
-- | Check if the given point is the given tile with the given color.
isPointMatchTile :: (Int, Int) -> Board -> Tile -> Bool
isPointMatchTile (x, y) board tile = tile == getTileAt (x, y) board

getTileAt :: (Int, Int) -> Board -> Tile
getTileAt (x, y) board
  | isPointOutOfBoard (x, y) board = Tile Nothing
  | otherwise                      = tile
  where
    (_, lowSplitY)   = splitAt y board
    row : _  = lowSplitY
    (_, rightSplitX) = splitAt x row
    tile : _ = rightSplitX
       
handleGame :: CodeWorld.Event -> State -> State
handleGame e@(PointerMovement mouse) s@(State _ _ True _ _) = s
handleGame e@(PointerMovement mouse) state =
  putTileAt (pointToCoords mouse) (
    changeDragAllowance e state
  )
handleGame e@(PointerPress mouse) state =
  putTileAt (pointToCoords mouse) (
    changeDragAllowance e state
  )
handleGame e@(PointerRelease mouse) s@(State _ _ True _ _) = s
handleGame e@(PointerRelease mouse) state =
  putTileAt (pointToCoords mouse) (
    changeDragAllowance e state
  )
handleGame _ s = s

changeDragAllowance :: CodeWorld.Event -> State -> State
changeDragAllowance (PointerPress _) (State a b c _ score) = State a b c True score
changeDragAllowance (PointerRelease _) (State a b c _ score) = State a b c False score
changeDragAllowance _ s = s

-- | Convert mouse position into board coordinates.
pointToCoords :: Point -> (Int, Int)
pointToCoords (x, y) = (round x, round (-y))

-- | Run a Point Match game with a sample starting board.
pointMatch :: [Board] -> IO ()
pointMatch board = activityOf state handleGame drawBoard
  where 
    state = State board (Tile Nothing) False False 0
    
enumerate :: [a] -> [(Int, a)]
enumerate = zip [0..]

-- | Sample board.
sampleBoard1 :: Board
sampleBoard1 =
  [ [ Key ColorRed, Key ColorOrange, Tile Nothing, Key ColorGreen, Key ColorYellow ]
  , [ Tile Nothing, Key ColorRed, Key ColorOrange, Tile Nothing, Tile Nothing ]
  , [ Key ColorGreen, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing ]
  , [ Key ColorYellow, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing ]
  , [ Key ColorBlue, Tile Nothing, Tile Nothing, Tile Nothing, Key ColorBlue ] ]

sampleBoard2 :: Board
sampleBoard2 =
  [ [ Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Key ColorBlue, Tile Nothing ]
  , [ Tile Nothing, Key ColorOrange, Key ColorPurple, Key ColorRed, Tile Nothing, Key ColorPink, Tile Nothing ]
  , [ Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing ]
  , [ Tile Nothing, Key ColorOrange, Tile Nothing, Tile Nothing, Tile Nothing, Key ColorPink, Tile Nothing ]
  , [ Tile Nothing, Key ColorPurple, Tile Nothing, Tile Nothing, Tile Nothing, Key ColorYellow, Tile Nothing ]
  , [ Tile Nothing, Key ColorGreen, Tile Nothing, Tile Nothing, Tile Nothing, Key ColorRed, Tile Nothing ]
  , [ Tile Nothing, Key ColorYellow, Tile Nothing, Key ColorGreen, Key ColorBlue, Tile Nothing, Tile Nothing ]
  ]

sampleBoard3 :: Board
sampleBoard3 =
  [ [ Tile Nothing, Tile Nothing, Tile Nothing, Key ColorRed, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Key ColorGray ]
  , [ Tile Nothing, Key ColorGreen, Tile Nothing, Key ColorPurple, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing ]
  , [ Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Key ColorRed, Tile Nothing, Key ColorPink, Key ColorBlue, Key ColorPurple ]
  , [ Tile Nothing, Tile Nothing, Key ColorBlue, Tile Nothing, Key ColorYellow, Tile Nothing, Key ColorBrown, Tile Nothing, Key ColorOrange ]
  , [ Tile Nothing, Tile Nothing, Key ColorPurple, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing ]
  , [ Tile Nothing, Tile Nothing, Key ColorYellow, Tile Nothing, Tile Nothing, Key ColorBlack, Key ColorBrown, Tile Nothing, Tile Nothing ]
  , [ Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Key ColorGreen, Tile Nothing, Tile Nothing ]
  , [ Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing ]
  , [ Key ColorOrange, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing, Tile Nothing ]
  ]

run :: IO()
run = pointMatch [sampleBoard1, sampleBoard2, sampleBoard3]